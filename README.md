# Text Styles

This is a basic app to display GNOME's in-development standard text styles.

It has been created with help from Builder, and that's where it's intended to 
be used from.

To make display the text styles, the app must but run in environment that has 
them; this currently means using GTK master or a version of GTK 3 with the 
changes manually applied.

See [this issue](https://gitlab.gnome.org/GNOME/gtk/issues/1808) for more 
details.
